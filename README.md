# Flood of tears

Code to create tears (drops) falling and filling the screen, using [Processing](https://processing.org/), a Java based programming language.

View in action in my video in [Youtube](https://youtu.be/fUEZRcjXwNU?t=167).
